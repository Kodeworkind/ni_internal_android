package nordicintent.com.yelp.data;

import javax.inject.Inject;
import javax.inject.Singleton;

import nordicintent.com.yelp.data.scopes.Local;
import nordicintent.com.yelp.data.scopes.Remote;
/**
 * Created by Vaibhav Barad on 4/6/2018.
 */

/**
 * <p>
 * By marking the constructor with {@code @Inject} and the class with {@code @Singleton}, Dagger
 * injects the dependencies required to create an instance of the Respository (if it fails, it
 * emits a compiler error). It uses {@link nordicintent.com.yelp.data.modules.RepositoryModule} to do so, and the constructed
 * instance is available in {@link nordicintent.com.yelp.di.AppComponent}.
 * <p/>
 * Dagger generated code doesn't require public access to the constructor or class, and
 * therefore, to ensure the developer doesn't instantiate the class manually and bypasses Dagger,
 * it's good practice minimise the visibility of the class/constructor as much as possible.
 */

@Singleton
public class Repository implements DataSource {
    private final DataSource mRemoteDataSource;
    private final DataSource mLocalDataSource;

    @Inject
    public Repository(@Remote DataSource mRemoteDataSource, @Local DataSource mLocalDataSource) {
        this.mRemoteDataSource = mRemoteDataSource;
        this.mLocalDataSource = mLocalDataSource;
    }
}