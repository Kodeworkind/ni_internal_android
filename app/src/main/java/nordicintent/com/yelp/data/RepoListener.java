package nordicintent.com.yelp.data;

/**
 * Created by Vaibhav Barad on 4/6/2018.
 */

public interface RepoListener<T> {
    void onSuccess(T t);

    void onFailure(String message);

    void redirectToLogin();
}
