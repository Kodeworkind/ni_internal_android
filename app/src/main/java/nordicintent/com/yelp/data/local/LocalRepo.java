package nordicintent.com.yelp.data.local;

import javax.inject.Inject;
import javax.inject.Singleton;

import nordicintent.com.yelp.data.DataSource;


/**
 * Concrete implementation of DataSource as a Realm/Room/Sqlite.
 */
@Singleton
public class LocalRepo implements DataSource {

    @Inject
    public LocalRepo() {

    }
}