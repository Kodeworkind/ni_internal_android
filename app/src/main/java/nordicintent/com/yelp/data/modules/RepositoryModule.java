package nordicintent.com.yelp.data.modules;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import nordicintent.com.yelp.data.DataSource;
import nordicintent.com.yelp.data.local.LocalRepo;
import nordicintent.com.yelp.data.remote.RemoteRepo;
import nordicintent.com.yelp.data.scopes.Local;
import nordicintent.com.yelp.data.scopes.Remote;

/**
 * This is used by Dagger to inject the required arguments into the {@link nordicintent.com.yelp.data.Repository}.
 */
@Module
abstract public class RepositoryModule {
    @Singleton
    @Binds
    @Local
    abstract DataSource provideLocalDataSource(LocalRepo dataSource);

    @Singleton
    @Binds
    @Remote
    abstract DataSource provideRemoteDataSource(RemoteRepo dataSource);

}