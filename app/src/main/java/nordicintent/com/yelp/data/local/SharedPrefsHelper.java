package nordicintent.com.yelp.data.local;

import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Vaibhav Barad on 3/29/2018.
 */
@Singleton
public class SharedPrefsHelper {
    public static String PREF_KEY_LOGIN_STATUS = "login_status";
    public static String PREF_USER_FDOC_ID = "user_firestore_document_id";
    public static String PREF_FIREBASE_TOKEN = "firebase_token";
    public static String PREF_FIREBASE_USER_ID = "firebase_user_id";

    /** User related preference key*/
    public static String PREF_USER_EMAIL = "user_email";

    private SharedPreferences mSharedPreferences;

    @Inject
    public SharedPrefsHelper(SharedPreferences sharedPreferences) {
        mSharedPreferences = sharedPreferences;
    }

    public void put(String key, String value) {
        mSharedPreferences.edit().putString(key, value).apply();
    }

    public void put(String key, int value) {
        mSharedPreferences.edit().putInt(key, value).apply();
    }

    public void put(String key, float value) {
        mSharedPreferences.edit().putFloat(key, value).apply();
    }

    public void put(String key, boolean value) {
        mSharedPreferences.edit().putBoolean(key, value).apply();
    }

    public String get(String key, String defaultValue) {
        return mSharedPreferences.getString(key, defaultValue);
    }

    public Integer get(String key, int defaultValue) {
        return mSharedPreferences.getInt(key, defaultValue);
    }

    public Float get(String key, float defaultValue) {
        return mSharedPreferences.getFloat(key, defaultValue);
    }

    public Boolean get(String key, boolean defaultValue) {
        return mSharedPreferences.getBoolean(key, defaultValue);
    }

    public void deleteSavedData(String key) {
        mSharedPreferences.edit().remove(key).apply();
    }

    public void clearSharedPreference() {
        mSharedPreferences.edit().clear().apply();
    }
}
