package nordicintent.com.yelp.data.remote;

import javax.inject.Inject;
import javax.inject.Singleton;

import nordicintent.com.yelp.data.API;
import nordicintent.com.yelp.data.DataSource;
import retrofit2.Retrofit;

/**
 * Implementation of the DataSource as Retrofit(Network Calls).
 */
@Singleton
public class RemoteRepo implements DataSource {
    Retrofit mRetrofit;
    private API api;
    public static final String TAG = RemoteRepo.class.getSimpleName();

    @Inject
    public RemoteRepo(Retrofit retrofit, API api) {
        this.mRetrofit = retrofit;
        this.api = api;
    }
}