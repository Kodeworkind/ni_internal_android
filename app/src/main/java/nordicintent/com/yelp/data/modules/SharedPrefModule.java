package nordicintent.com.yelp.data.modules;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import nordicintent.com.yelp.BuildConfig;
import nordicintent.com.yelp.data.local.SharedPrefsHelper;

/**
 * Created by Vaibhav Barad on 3/29/2018.
 */
@Module
public class SharedPrefModule {

    @Singleton
    @Provides
    SharedPreferences provideSharedPrefs(Application application) {
        return application.getSharedPreferences(BuildConfig.APP_PREFS, Context.MODE_PRIVATE);
    }

    @Singleton
    @Provides
    SharedPrefsHelper provideSharedPrefsHelper(SharedPreferences preferences) {
        return new SharedPrefsHelper(preferences);
    }

}
