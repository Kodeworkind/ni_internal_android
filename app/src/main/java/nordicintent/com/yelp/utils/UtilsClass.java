package nordicintent.com.yelp.utils;

import android.net.Uri;

import nordicintent.com.yelp.R;

/**
 * Created by Vaibhav Barad on 4/6/2018.
 */

public class UtilsClass {

    public static String getURLForResource (int resourceId) {
        return Uri.parse("android.resource://"+R.class.getPackage().getName()+"/" +resourceId).toString();
    }
}
