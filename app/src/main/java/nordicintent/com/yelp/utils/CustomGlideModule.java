package nordicintent.com.yelp.utils;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;


/**
 * Created by Vaibhav Barad on 3/29/2018.
 */

@GlideModule
public class CustomGlideModule extends AppGlideModule {
}
