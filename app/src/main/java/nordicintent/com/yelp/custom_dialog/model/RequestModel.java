package nordicintent.com.yelp.custom_dialog.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Vaibhav Barad on 4/6/2018.
 */

public class RequestModel implements Parcelable {

    String image_url;
    int id;

    public RequestModel(String image_url, int id) {
        this.image_url = image_url;
        this.id = id;
    }

    protected RequestModel(Parcel in) {
        image_url = in.readString();
        id = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(image_url);
        dest.writeInt(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RequestModel> CREATOR = new Creator<RequestModel>() {
        @Override
        public RequestModel createFromParcel(Parcel in) {
            return new RequestModel(in);
        }

        @Override
        public RequestModel[] newArray(int size) {
            return new RequestModel[size];
        }
    };

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
