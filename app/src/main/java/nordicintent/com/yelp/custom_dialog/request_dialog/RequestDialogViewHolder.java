package nordicintent.com.yelp.custom_dialog.request_dialog;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import nordicintent.com.yelp.R;
import nordicintent.com.yelp.custom_dialog.model.RequestModel;
import nordicintent.com.yelp.utils.GlideApp;

/**
 * Created by Vaibhav Barad on 4/6/2018.
 */

public class RequestDialogViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.iv_request_icon)
    ImageView ivRequestIcon;

    public RequestDialogViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(RequestModel model, RequestDialogAdapter.IRequestSelected iRequestListener) {
        GlideApp.with(ivRequestIcon.getContext()).load(model.getImage_url()).into(ivRequestIcon);
        itemView.setOnClickListener(view->{ if (iRequestListener != null) {
            iRequestListener.OnRequestSelected(itemView,getAdapterPosition());
        }});
    }
}
