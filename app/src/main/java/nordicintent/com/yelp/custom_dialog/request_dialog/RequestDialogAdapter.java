package nordicintent.com.yelp.custom_dialog.request_dialog;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import nordicintent.com.yelp.R;
import nordicintent.com.yelp.custom_dialog.model.RequestModel;

/**
 * Created by Vaibhav Barad on 4/6/2018.
 */

public class RequestDialogAdapter extends RecyclerView.Adapter<RequestDialogViewHolder> {

    IRequestSelected iRequestListener;
    ArrayList<RequestModel> requestlist;


    public RequestDialogAdapter(IRequestSelected iRequestListener, ArrayList<RequestModel> requestlist) {
        this.iRequestListener = iRequestListener;
        this.requestlist = requestlist;
    }

    @Override
    public RequestDialogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.request_item, parent, false);
        return new RequestDialogViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return requestlist.size();
    }

    @Override
    public void onBindViewHolder(RequestDialogViewHolder holder, int position) {
        RequestModel model = requestlist.get(position);
        holder.bind(model, iRequestListener);
    }

    public interface IRequestSelected {
        void OnRequestSelected(View view, int position);
    }
}
