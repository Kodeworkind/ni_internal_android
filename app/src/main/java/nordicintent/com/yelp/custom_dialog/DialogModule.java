package nordicintent.com.yelp.custom_dialog;

import dagger.Binds;
import dagger.Module;
import nordicintent.com.yelp.custom_dialog.request_dialog.RequestDialog;
import nordicintent.com.yelp.di.FragmentScoped;

/**
 * Created by Vaibhav Barad on 4/6/2018.
 */
@Module
public abstract class DialogModule {

    @Binds
    @FragmentScoped
    abstract RequestDialog requestDialog(RequestDialog presenter);
}
