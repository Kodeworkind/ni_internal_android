package nordicintent.com.yelp.custom_dialog.request_dialog;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import nordicintent.com.yelp.R;
import nordicintent.com.yelp.custom_dialog.model.RequestModel;
import nordicintent.com.yelp.di.FragmentScoped;

import static nordicintent.com.yelp.utils.UtilsClass.getURLForResource;

/**
 * Created by Vaibhav Barad on 4/6/2018.
 */
@FragmentScoped
public class RequestDialog extends DialogFragment implements RequestDialogAdapter.IRequestSelected {

    public static final String TAG = "RequestDialogTag";

    @BindView(R.id.et_comment)
    EditText etComment;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.parentView)
    ConstraintLayout parentView;
    Unbinder unbinder;
    @BindView(R.id.tv_go)
    TextView tvGo;
    private DialogListener listener;

    RequestModel requestClicked;
    RequestDialogAdapter mRequestDialogAdapter;

    static ArrayList<RequestModel> requestList;


    @Override
    public void OnRequestSelected(View view, int position) {
        requestClicked = requestList.get(position);
    }

    public interface DialogListener {
        void onDialogGoClick(String commentMessage, RequestModel requestClicked);
    }

    public static RequestDialog newInstance( ArrayList<RequestModel> requestList) {
        Bundle args = new Bundle();
        args.putParcelableArrayList("request_data",requestList);

        RequestDialog fragment = new RequestDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        if (window != null) {
            window.requestFeature(Window.FEATURE_NO_TITLE);
        }
        dummyData();

        View view = inflater.inflate(R.layout.request_dialog, container, false);
        unbinder = ButterKnife.bind(this, view);
        getDialog().setCanceledOnTouchOutside(true);
        return view;
    }

    private static void dummyData() {
        requestList = new ArrayList<>();
        requestList.add(new RequestModel(getURLForResource(R.drawable.coffee),1));
        requestList.add(new RequestModel(getURLForResource(R.drawable.house_keeping),2));
        requestList.add(new RequestModel(getURLForResource(R.drawable.hardware),3));
        requestList.add(new RequestModel(getURLForResource(R.drawable.hardware),4));
        requestList.add(new RequestModel(getURLForResource(R.drawable.hardware),5));
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        requestList = getArguments().getParcelableArrayList("request_data");
        dummyData();

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRequestDialogAdapter = new RequestDialogAdapter(RequestDialog.this,requestList);
        recyclerView.setAdapter(mRequestDialogAdapter);
    }

    @OnClick(R.id.tv_go)
    public void onButtonClicked() {
        closeDialog();
        if (listener != null) {
            listener.onDialogGoClick(tvGo.getText().toString(), requestClicked);
        }
    }

    public void closeDialog() {
        if (getDialog().isShowing()) {
            closeKeyboard();
            getDialog().dismiss();
        }
    }

    protected void closeKeyboard() {
        InputMethodManager imm = (InputMethodManager)
                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(
                getActivity().findViewById(android.R.id.content).getWindowToken(), 0);
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            listener = (DialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement RequestDialogFragment.DialogListener");
        }
    }
}