package nordicintent.com.yelp.di;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import nordicintent.com.yelp.YelpApp;
import nordicintent.com.yelp.data.modules.NetworkModule;
import nordicintent.com.yelp.data.modules.RepositoryModule;
import nordicintent.com.yelp.data.modules.SharedPrefModule;

/**
 * Created by Vaibhav Barad on 4/6/2018.
 */

/**
 * This is a Dagger component. Refer to {@link nordicintent.com.yelp.YelpApp} for the list of Dagger components
 * used in this application.
 * <p>
 * Even though Dagger allows annotating a {@link Component} as a singleton, the code
 * itself must ensure only one instance of the class is created. This is done in {@link
 * nordicintent.com.yelp.YelpApp}.
 * <p>
 * {@link AndroidSupportInjectionModule} is the module from Dagger.Android that helps with the generation
 * and location of subcomponents.
 */
@Singleton
@Component(modules = {
        RepositoryModule.class,
        ApplicationModule.class,
        NetworkModule.class,
        SharedPrefModule.class,
        ActivityBindingModule.class,
        AndroidSupportInjectionModule.class})
public interface AppComponent extends AndroidInjector<YelpApp> {
    //Repository getRepository();

    // Gives us syntactic sugar. we can then do DaggerAppComponent.builder().application(this).build().inject(this);
    // never having to instantiate any modules or say which module we are passing the application to.
    // Application will just be provided into our app graph now.
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

    void inject(YelpApp app);
}